#/bin/bash
pushd /workspace 
git clone https://github.com/owid/owid-content
popd

docker-compose up -d

# wait for mysql to start
MYSQLCMD='mysql -h localhost -u root --protocol tcp'
while ! $MYSQLCMD -e 'select 1'; do
  echo "Waiting for mysql... Ignore error system error: 107, as it means mysql is not available yet"
  sleep 10
done


echo "Altering mysql root user"
echo "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY ''; flush privileges;" | $MYSQLCMD

echo "Loading database..."
$MYSQLCMD -e "CREATE DATABASE owid;"
curl -Lo /tmp/owid_metadata.sql.gz https://files.ourworldindata.org/owid_metadata.sql.gz
gunzip < /tmp/owid_metadata.sql.gz | $MYSQLCMD -D owid
curl -Lo /tmp/owid_chartdata.sql.gz https://files.ourworldindata.org/owid_chartdata.sql.gz
gunzip < /tmp/owid_chartdata.sql.gz | $MYSQLCMD -D owid

echo "Database loaded."


echo "Running yarn install"

yarn

echo "Building and starting application"

cp .env.example .env
